use crate::enums::app_error::AppError;

pub const MESSAGE_TYPE_GET_OFFERS: &str = "get-offers";
pub const MESSAGE_TYPE_GET_OFFERS_RESPONSE: &str = "get-offers-response";
pub const MESSAGE_TYPE_OFFER: &str = "offer";
pub const MESSAGE_TYPE_ANSWER: &str = "answer";
pub const MESSAGE_TYPE_OFFER_CANCEL: &str = "offer-cancel";
pub const MESSAGE_TYPE_OFFER_REJECT: &str = "offer-reject";
pub const MESSAGE_TYPE_NEW_ICE_CANDIDATE: &str = "new-ice-candidate";

#[derive(PartialEq, Debug, Copy, Clone)]
pub enum MessageType {
    GetOffers,
    GetOffersResponse,
    Offer,
    Answer,
    OfferCancel,
    OfferReject,
    NewIceCandidate,
}

impl MessageType {
    pub fn from_str(string:&str) -> Result<MessageType, AppError> {
        let result = match string {
            MESSAGE_TYPE_GET_OFFERS => MessageType::GetOffers,
            MESSAGE_TYPE_OFFER => MessageType::Offer,
            MESSAGE_TYPE_ANSWER => MessageType::Answer,
            MESSAGE_TYPE_OFFER_REJECT => MessageType::OfferReject,
            MESSAGE_TYPE_OFFER_CANCEL => MessageType::OfferCancel,
            MESSAGE_TYPE_NEW_ICE_CANDIDATE => MessageType::NewIceCandidate,
            _ => return Err(AppError::UnknownMessage),
        };

        Ok(result)
    }
}