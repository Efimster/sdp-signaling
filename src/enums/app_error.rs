use std::io;
use std::str::Utf8Error;
use std::sync::mpsc::SendError;
use crate::structures::notification_hub::Message;
use std::sync::mpsc::TryRecvError;
use json::JSONError;
use websockets::enums::web_socket_error::WebSocketError;


#[derive(Debug, Clone)]
pub enum AppError {
    SocketReadTimeout,
    ChannelReadTimeout,
    WebSocket(WebSocketError),
    Json(JSONError),
    ChannelDisconnected,
    IO(io::ErrorKind),
    Terminate,
    TargetChannelLoop,
    TargetChannelDoesNotExist,
    UnexpectedMessageType,
    Utf8Error(Utf8Error),
    UnknownMessage,
}

impl From<io::Error> for AppError {
    fn from(error: io::Error) -> Self {
        match error.kind() {
            io::ErrorKind::WouldBlock | io::ErrorKind::TimedOut => AppError::SocketReadTimeout,
            _ => AppError::IO(error.kind())
        }
    }
}

impl From<Utf8Error> for AppError {
    fn from(error: Utf8Error) -> Self {
        AppError::Utf8Error(error)
    }
}

impl From<SendError<Message>> for AppError {
    fn from(_: SendError<Message>) -> Self {
        AppError::ChannelDisconnected
    }
}

impl From<TryRecvError> for AppError {
    fn from(error: TryRecvError) -> Self {
        match error {
            TryRecvError::Empty => AppError::ChannelReadTimeout,
            TryRecvError::Disconnected => AppError::ChannelDisconnected
        }
    }
}

impl From<JSONError> for AppError {
    fn from(error: JSONError) -> Self {
        AppError::Json(error)
    }
}

impl From<WebSocketError> for AppError {
    fn from(error: WebSocketError) -> Self {
        AppError::WebSocket(error)
    }
}