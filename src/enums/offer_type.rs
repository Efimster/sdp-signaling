pub const OFFER_TYPE_REQUEST: &str = "offer";
pub const OFFER_TYPE_ANSWER: &str = "answer";

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum OfferType {
    Request,
    Answer
}