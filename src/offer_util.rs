use crate::enums::message_type::MessageType;
use crate::enums::offer_type::OfferType;
use crate::structures::messages::offer_message::OfferMessage;
use crate::structures::offer::Offer;
use crate::enums::app_error::AppError;

pub fn offer_type_from_message_type(message_type:MessageType) -> Result<OfferType, AppError> {
    match  message_type {
        MessageType::Offer => Ok(OfferType::Request),
        MessageType::Answer => Ok(OfferType::Answer),
        _ => Err(AppError::UnexpectedMessageType),
    }
}


pub fn create_offer_from_message(message:&OfferMessage) -> Result<Offer, AppError> {
    let offer_type = offer_type_from_message_type(message.message_type)?;
    Ok(Offer{
        offer_type,
        originator: message.originator.clone(),
        counterpart: message.counterpart.clone(),
        sdp: message.sdp.clone()
    })

}