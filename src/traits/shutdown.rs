use std::net::{TcpStream, Shutdown as TcpShutdown};
use std::io;

pub trait Shutdown {
    fn shutdown(&self) -> io::Result<()>;
}

impl Shutdown for TcpStream {
    fn shutdown(&self) -> io::Result<()> {
        self.shutdown(TcpShutdown::Both)?;
        Ok(())
    }
}