
use std::net::{TcpListener};
use std::str;
use std::sync::{Arc, RwLock};
mod structures;
mod enums;
mod traits;
mod offer_util;
use crate::structures::{thread_pool::ThreadPool,offer_catalog2::OfferCatalog, notification_hub::NotificationHub, connection::Connection};
use websockets::structures::web_socket_stream::WebSocketStream;
use x509::utils::file_util::{read_certificate_file, read_private_key};
use std::time::Duration;


const LISTENING_SOCKET_ADDR:&str = "127.0.0.1:7878";

fn main() {
    let mut certificates_chain: Vec<&[u8]> = Vec::with_capacity(2);
    let server_certificate = read_certificate_file("res/trytls2.cer").expect("< can't read certificate");
    let ca_certificate = read_certificate_file("res/trytlsca.cer").expect("< can't read ca certificate");
    certificates_chain.push(&server_certificate);
    certificates_chain.push(&ca_certificate);
    let private_key = read_private_key("res/ecdsa-private-key.pem", "ecdsa");

    let offer_catalog =  Arc::new(RwLock::new(OfferCatalog::create()));
    let notification_hub = NotificationHub::create();
    let listener = TcpListener::bind(LISTENING_SOCKET_ADDR).unwrap();
    let pool = ThreadPool::new(2);
    println!("listen {}", LISTENING_SOCKET_ADDR);

    for stream in WebSocketStream::incoming_secure(&listener, &certificates_chain, &private_key).into_iter() {
        match stream {
            Ok(stream) => {
                println!("Incoming connection {}", stream.peer_addr().unwrap().ip());
                stream.set_read_timeout(Some(Duration::from_millis(50))).unwrap();

                let mut connection = Connection {
                    reader: stream.reader,
                    writer: stream.writer,
                    offer_catalog: Arc::clone(&offer_catalog),
                    notification_hub: Arc::clone(&notification_hub),
                    channel: None,
                };

                pool.execute(move || {
                   match connection.connection_loop() {
                        Ok(()) => (),
                        Err(err) => println!("app error: {:?}", err)
                   }
                });
            }
            Err(error) => println!("connection establishment error: {:?}", error),
        }
    }
    notification_hub.read().unwrap().terminate();
}
