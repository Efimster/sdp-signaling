use crate::enums::message_type::{MessageType, MESSAGE_TYPE_OFFER, MESSAGE_TYPE_ANSWER};
use json::{JSON, JSONError};
use crate::enums::app_error::AppError;

#[derive(Debug)]
pub struct OfferMessage {
    pub message_id: u64,
    pub message_type: MessageType,
    pub originator: String,
    pub counterpart: String,
    pub sdp: JSON
}

impl OfferMessage {
    pub fn to_json(&self) -> JSON {
        let r#type = match self.message_type {
            MessageType::Answer => JSON::JSONString(MESSAGE_TYPE_ANSWER.to_string()),
            _ => JSON::JSONString(MESSAGE_TYPE_OFFER.to_string()),
        };

        JSON::new_object()
            .add_key_value("messageId", JSON::JSONNumber(self.message_id as f64)).unwrap()
            .add_key_value("type", r#type).unwrap()
            .add_key_value("originator", JSON::JSONString(self.originator.clone())).unwrap()
            .add_key_value("counterpart", JSON::JSONString(self.counterpart.clone())).unwrap()
            .add_key_value("sdp", self.sdp.clone()).unwrap()
    }

    pub fn from_json(json: JSON) -> Result<OfferMessage, AppError> {
        let message_id: u64;
        let message_type: MessageType;
        let originator: String;
        let counterpart: String;
        let sdp: JSON;

        let error = AppError::Json(JSONError::Parse);

        match json {
            JSON::JSONObject(members) => {
                if let Some(JSON::JSONNumber(value)) = members.get("messageId") {
                    message_id = *value as u64;
                } else {
                    return Err(error);
                }
                if let Some(JSON::JSONString(value)) = members.get("type") {
                    message_type = match value.as_str() {
                        MESSAGE_TYPE_OFFER => MessageType::Offer,
                        MESSAGE_TYPE_ANSWER => MessageType::Answer,
                        _ => return Err(error)
                    }
                } else {
                    return Err(error);
                }
                if let Some(JSON::JSONString(value)) = members.get("originator") {
                    originator = value.clone();
                } else {
                    return Err(error);
                }
                if let Some(JSON::JSONString(value)) = members.get("counterpart") {
                    counterpart = value.clone();
                } else {
                    return Err(error);
                }
                if let Some(value) = members.get("sdp") {
                    sdp = value.clone();
                } else {
                    return Err(error);
                }
            }
            _ => return Err(error)
        };

        Ok(OfferMessage {
            message_id,
            message_type,
            originator,
            counterpart,
            sdp
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::collections::HashMap;

    #[test]
    fn test_to_json() {
        let sdp = "{\"sdp\":\"v = 0\r{}}\r\n\"}";
        let message = OfferMessage {
            message_id: 123,
            message_type: MessageType::Offer,
            originator: String::from("origin"),
            counterpart: String::from("foreign address"),
            sdp: JSON::parse(sdp).unwrap().clone(),
        };

        let json = message.to_json();
        if let JSON::JSONObject(members) = json {
            if let Some(value) = members.get("sdp") {
                assert_eq!(value.stringify(), sdp);
            }
            else {
                panic!("unexpected JSON of OfferMessage.sdp");
            }
        }
        else {
            panic!("unexpected JSON of OfferMessage");
        }
    }

    #[test]
    fn test_from_json() {
        let sdp = "{\"sdp\":\"v = 0\r{}}\r\n\"}";
        let mut members: HashMap<String, JSON> = HashMap::new();
        members.insert(String::from("messageId"), JSON::JSONNumber(22f64));
        members.insert(String::from("type"), JSON::JSONString(String::from(MESSAGE_TYPE_OFFER)));
        members.insert(String::from("originator"), JSON::JSONString(String::from("sameorigin")));
        members.insert(String::from("counterpart"), JSON::JSONString(String::from("foreign address")));
        members.insert(String::from("sdp"), JSON::parse(sdp).unwrap());

        let message = OfferMessage::from_json(JSON::JSONObject(members)).unwrap();
        assert_eq!(message.message_id, 22);
        assert_eq!(message.message_type, MessageType::Offer);
        assert_eq!(message.originator, "sameorigin");
        assert_eq!(message.counterpart, "foreign address");
        assert_eq!(message.sdp.stringify(), sdp);
    }
}


