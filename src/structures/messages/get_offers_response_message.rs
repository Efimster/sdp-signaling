use std::collections::HashMap;
use crate::enums::message_type::{MessageType, MESSAGE_TYPE_GET_OFFERS_RESPONSE};
use crate::structures::offer::Offer;
use json::JSON;

pub struct GetOffersResponseMessage<'a> {
    pub message_id: u64,
    pub message_type: MessageType,
    pub offers: Vec<&'a Offer>,
}

impl<'a> GetOffersResponseMessage<'_> {
    pub fn to_json(&self) -> JSON {
        let mut members: HashMap<String, JSON> = HashMap::new();
        members.insert(String::from("messageId"), JSON::JSONNumber(self.message_id as f64));
        members.insert(String::from("type"), JSON::JSONString(String::from(MESSAGE_TYPE_GET_OFFERS_RESPONSE)));

        let offers = self.offers.iter().map(|offer| offer.to_json()).collect::<Vec<_>>();
        members.insert(String::from("offers"), JSON::JSONArray(offers));

        JSON::JSONObject(members)
    }
}


