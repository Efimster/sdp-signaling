use crate::enums::message_type::MessageType;
use json::{JSON, JSONError};
use crate::enums::app_error::AppError;

pub struct GetOffersMessage {
    pub message_id:u64,
    pub message_type:MessageType,
    pub originator:String,
}

impl GetOffersMessage {
    pub fn from_json(json:JSON) -> Result<GetOffersMessage, AppError> {
        let message_id:u64;
        let message_type:MessageType = MessageType::GetOffers;
        let originator:String;

        let error = AppError::Json(JSONError::Parse);

        match json {
            JSON::JSONObject(members) => {
                if let Some(JSON::JSONNumber(value)) = members.get("messageId") {
                    message_id = *value as u64;
                }
                else {
                    return Err(error.clone());
                }

                if let Some(JSON::JSONString(value)) = members.get("originator"){
                    originator = value.clone();
                }
                else {
                    return Err(error);
                }
            },
            _ => return Err(error)
        };

        Ok(GetOffersMessage {
            message_id,
            message_type,
            originator
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::collections::HashMap;
    use crate::enums::message_type::MESSAGE_TYPE_GET_OFFERS;

    #[test]
    fn test_from_json(){
        let mut members: HashMap<String, JSON> = HashMap::new();
        members.insert(String::from("messageId"), JSON::JSONNumber(22f64));
        members.insert(String::from("type"), JSON::JSONString(String::from(MESSAGE_TYPE_GET_OFFERS)));
        members.insert(String::from("originator"), JSON::JSONString(String::from("sameorigin")));

        let message = GetOffersMessage::from_json(JSON::JSONObject(members)).unwrap();
        assert_eq!(message.message_id, 22);
        assert_eq!(message.message_type, MessageType::GetOffers);
        assert_eq!(message.originator, "sameorigin");
    }
}


