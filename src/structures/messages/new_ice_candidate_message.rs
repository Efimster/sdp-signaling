use crate::enums::message_type::{MessageType, MESSAGE_TYPE_NEW_ICE_CANDIDATE};
use json::{JSON, JSONError};
use crate::enums::app_error::AppError;

#[derive(Debug)]
pub struct NewIceCandidateMessage {
    pub message_id: u64,
    pub message_type: MessageType,
    pub originator: String,
    pub counterpart: String,
    pub candidate: JSON
}

impl NewIceCandidateMessage {
    pub fn to_json(&self) -> JSON {
        JSON::new_object()
            .add_key_value("messageId", JSON::JSONNumber(self.message_id as f64)).unwrap()
            .add_key_value("type", JSON::JSONString(MESSAGE_TYPE_NEW_ICE_CANDIDATE.to_string())).unwrap()
            .add_key_value("originator", JSON::JSONString(self.originator.clone())).unwrap()
            .add_key_value("counterpart", JSON::JSONString(self.counterpart.clone())).unwrap()
            .add_key_value("candidate", self.candidate.clone()).unwrap()
    }

    pub fn from_json(json: JSON) -> Result<NewIceCandidateMessage, AppError> {
        let message_id: u64;
        let message_type: MessageType = MessageType::NewIceCandidate;
        let originator: String;
        let counterpart: String;
        let candidate: JSON;

        let error = AppError::Json(JSONError::Parse);

        match json {
            JSON::JSONObject(members) => {
                if let Some(JSON::JSONNumber(value)) = members.get("messageId") {
                    message_id = *value as u64;
                } else {
                    return Err(error);
                }
                if let Some(JSON::JSONString(value)) = members.get("originator") {
                    originator = value.clone();
                } else {
                    return Err(error);
                }
                if let Some(JSON::JSONString(value)) = members.get("counterpart") {
                    counterpart = value.clone();
                } else {
                    return Err(error);
                }
                if let Some(value) = members.get("candidate") {
                    candidate = value.clone();
                } else {
                    return Err(error);
                }
            }
            _ => return Err(error)
        };

        Ok(NewIceCandidateMessage {
            message_id,
            message_type,
            originator,
            counterpart,
            candidate
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_to_json() {
        let candidate = "{\"candidate\":\"v = 0\r{}}\r\n\"}";
        let message = NewIceCandidateMessage {
            message_id: 123,
            message_type: MessageType::Offer,
            originator: String::from("origin"),
            counterpart: String::from("foreign address"),
            candidate: JSON::parse(candidate).unwrap().clone(),
        };

        let compare = JSON::new_object()
            .add_key_value("messageId", JSON::JSONNumber(123f64)).unwrap()
            .add_key_value("type", JSON::JSONString(MESSAGE_TYPE_NEW_ICE_CANDIDATE.to_string())).unwrap()
            .add_key_value("originator", JSON::JSONString("origin".to_string())).unwrap()
            .add_key_value("counterpart", JSON::JSONString("foreign address".to_string())).unwrap()
            .add_key_value("candidate", JSON::parse(candidate).unwrap()).unwrap();

        assert_eq!(message.to_json(), compare);
    }

    #[test]
    fn test_from_json() {
        let candidate = "{\"candidate\":\"v = 0\r{}}\r\n\"}";

        let json = JSON::new_object()
            .add_key_value("messageId", JSON::JSONNumber(22f64)).unwrap()
            .add_key_value("type", JSON::JSONString(String::from(MESSAGE_TYPE_NEW_ICE_CANDIDATE))).unwrap()
            .add_key_value("originator", JSON::JSONString(String::from("sameorigin"))).unwrap()
            .add_key_value("counterpart", JSON::JSONString(String::from("foreign address"))).unwrap()
            .add_key_value("candidate", JSON::parse(candidate).unwrap()).unwrap();

        let message = NewIceCandidateMessage::from_json(json).unwrap();
        assert_eq!(message.message_id, 22);
        assert_eq!(message.message_type, MessageType::NewIceCandidate);
        assert_eq!(message.originator, "sameorigin");
        assert_eq!(message.counterpart, "foreign address");
        assert_eq!(message.candidate.stringify(), candidate);
    }
}


