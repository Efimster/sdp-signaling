use std::collections::HashMap;
use crate::enums::message_type::{MessageType, MESSAGE_TYPE_OFFER_CANCEL};
use json::{JSON, JSONError};
use crate::enums::app_error::AppError;

#[derive(Debug)]
pub struct CancelOfferMessage {
    pub message_id: u64,
    pub message_type: MessageType,
    pub originator: String,
    pub counterpart: String,
}

impl CancelOfferMessage {
    pub fn to_json(&self) -> JSON {
        let mut members: HashMap<String, JSON> = HashMap::new();
        members.insert(String::from("messageId"), JSON::JSONNumber(self.message_id as f64));
        members.insert(String::from("type"), JSON::JSONString(MESSAGE_TYPE_OFFER_CANCEL.to_string()));
        members.insert(String::from("originator"), JSON::JSONString(self.originator.clone()));
        members.insert(String::from("counterpart"), JSON::JSONString(self.counterpart.clone()));

        JSON::JSONObject(members)
    }

    pub fn from_json(json: JSON) -> Result<CancelOfferMessage, AppError> {
        let message_id: u64;
        let originator: String;
        let counterpart: String;
        let message_type: MessageType = MessageType::OfferCancel;

        let error = AppError::Json(JSONError::Parse);

        match json {
            JSON::JSONObject(members) => {
                if let Some(JSON::JSONNumber(value)) = members.get("messageId") {
                    message_id = *value as u64;
                } else {
                    return Err(error);
                }
                if let Some(JSON::JSONString(value)) = members.get("originator") {
                    originator = value.clone();
                } else {
                    return Err(error);
                }
                if let Some(JSON::JSONString(value)) = members.get("counterpart") {
                    counterpart = value.clone();
                } else {
                    return Err(error);
                }
            }
            _ => return Err(error)
        };

        Ok(CancelOfferMessage {
            message_id,
            message_type,
            originator,
            counterpart,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_to_json() {
        let message = CancelOfferMessage {
            message_id: 123,
            message_type: MessageType::OfferCancel,
            originator: String::from("origin"),
            counterpart: String::from("foreign address"),
        };

        match message.to_json() {
            JSON::JSONObject(_) => (),
            _ => panic!("unexpected JSON of OfferMessage")
        }
    }

    #[test]
    fn test_from_json() {
        let mut members: HashMap<String, JSON> = HashMap::new();
        members.insert(String::from("messageId"), JSON::JSONNumber(22f64));
        members.insert(String::from("type"), JSON::JSONString(String::from(MESSAGE_TYPE_OFFER_CANCEL)));
        members.insert(String::from("originator"), JSON::JSONString(String::from("sameorigin")));
        members.insert(String::from("counterpart"), JSON::JSONString(String::from("foreign address")));

        let message = CancelOfferMessage::from_json(JSON::JSONObject(members)).unwrap();
        assert_eq!(message.message_id, 22);
        assert_eq!(message.message_type, MessageType::OfferCancel);
        assert_eq!(message.originator, "sameorigin");
        assert_eq!(message.counterpart, "foreign address");
    }
}


