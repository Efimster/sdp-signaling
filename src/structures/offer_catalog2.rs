use crate::structures::offer::Offer;
use crate::enums::app_error::AppError;

pub struct OfferCatalog {
    pub offers: Vec<Offer>,
}

impl OfferCatalog {
    pub fn create() -> OfferCatalog {
        OfferCatalog {
            offers: Vec::new(),
        }
    }

    ///adds offer to catalog
    pub fn insert_request(&mut self, offer: Offer) -> Result<(), AppError> {
       self.offers.push(offer);
       Ok(())
    }

    ///removes paired offer from catalog
    pub fn remove_offer(&mut self, originator: &str, counterpart: &str) -> Result<(), AppError> {
        let remove_indexes = self.offers.iter().enumerate().filter_map(|(index, offer)| {
            if &offer.originator == originator && &offer.counterpart == counterpart {
                Some(index)
            }
            else {
                None
            }
        })
        .collect::<Vec<_>>();

//        println!("remove_indexes {:?}, to {}", remove_indexes, self.offers[remove_indexes[0]].counterpart);

       remove_indexes.iter().rev().for_each(|&index| {self.offers.remove(index);});
        Ok(())
    }

    pub fn get_offers_for(&self, name: &str) -> Vec<&Offer> {
        self.offers.iter().filter(|&offer| &offer.originator == name || &offer.counterpart == name).collect::<Vec<_>>()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::enums::offer_type::OfferType;
    use json::JSON;

    #[test]
    fn test_insert_offer_from_non_existing_originator() {
        let originator = "nick";
        let counterpart = "vick";
        let offer = Offer {
            offer_type: OfferType::Request,
            originator: String::from(originator),
            counterpart: String::from(counterpart),
            sdp: JSON::Null,
        };

        let mut catalog = OfferCatalog::create();
        catalog.insert_request(offer).unwrap();
        assert_eq!(catalog.offers.len(), 1);
        assert_eq!(catalog.offers[0].originator, originator);
        assert_eq!(catalog.offers[0].counterpart, counterpart);
    }

    #[test]
    fn test_insert_offer_from_existing_originator() {
        let originator = "nick";
        let counterpart = "vick";
        let offer = Offer {
            offer_type: OfferType::Request,
            originator: String::from(originator),
            counterpart: String::from(counterpart),
            sdp: JSON::Null,
        };

        let mut catalog = OfferCatalog::create();
        catalog.insert_request(offer).unwrap();

        let counterpart2 = "dick";
        let offer2 = Offer {
            offer_type: OfferType::Request,
            originator: String::from(originator),
            counterpart: String::from(counterpart2),
            sdp: JSON::Null
        };
        catalog.insert_request(offer2).unwrap();

        assert_eq!(catalog.offers.len(), 2);
        assert_eq!(catalog.offers[1].originator, originator);
        assert_eq!(catalog.offers[1].counterpart, counterpart2);
    }

    #[test]
    fn test_remove_offer() {
        let mut catalog = OfferCatalog::create();
        let originator = "nick";
        let counterpart = "vick";
        let offer = Offer {
            offer_type: OfferType::Request,
            originator: String::from(originator),
            counterpart: String::from(counterpart),
            sdp: JSON::Null,
        };

        catalog.insert_request(offer).unwrap();

        let counterpart2 = "dick";
        let offer2 = Offer {
            offer_type: OfferType::Request,
            originator: String::from(originator),
            counterpart: String::from(counterpart2),
            sdp: JSON::Null
        };
        catalog.insert_request(offer2).unwrap();


        catalog.remove_offer(originator, counterpart).unwrap();
        assert_eq!(catalog.offers.len(), 1);
        assert_eq!(catalog.offers[0].counterpart, counterpart2);

        catalog.remove_offer(originator, counterpart2).unwrap();
        assert_eq!(catalog.offers.len(), 0);
    }

    #[test]
    fn test_get_offers_for() {
        let originator = "nick";
        let counterpart = "vick";
        let offer = Offer {
            offer_type: OfferType::Request,
            originator: String::from(originator),
            counterpart: String::from(counterpart),
            sdp: JSON::Null,
        };

        let mut catalog = OfferCatalog::create();
        catalog.insert_request(offer).unwrap();

        let counterpart2 = "dick";
        let offer2 = Offer {
            offer_type: OfferType::Request,
            originator: String::from(originator),
            counterpart: String::from(counterpart2),
            sdp: JSON::Null
        };
        catalog.insert_request(offer2).unwrap();

        let offers = catalog.get_offers_for("vick");
        assert_eq!(offers.len(), 1);
        assert_eq!(catalog.offers.len(), 2);

        let offers = catalog.get_offers_for("nick");
        assert_eq!(offers.len(), 2);
    }
}