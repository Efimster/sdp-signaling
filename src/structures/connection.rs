use std::sync::{Arc, RwLock};
use crate::structures::offer_catalog2::OfferCatalog;
use crate::structures::notification_hub::{NotificationHub, PeerChannel};
use websockets::structures::web_socket_reader::WebSocketReader;
use std::io::{Read, Write};
use websockets::enums::message::Message;
use json::{JSON, JSONError};
use crate::structures::messages::new_ice_candidate_message::NewIceCandidateMessage;
use crate::structures::messages::cancel_offer_message::CancelOfferMessage;
use crate::structures::messages::reject_offer_message::RejectOfferMessage;
use crate::structures::messages::offer_message::OfferMessage;
use crate::structures::messages::get_offers_message::GetOffersMessage;
use crate::structures::messages::get_offers_response_message::GetOffersResponseMessage;
use crate::enums::message_type::MessageType;
use websockets::enums::web_socket_error::WebSocketError;
use crate::offer_util;
use std::{str, io};
use crate::structures::notification_hub::Message as NotificationMessage;
use websockets::structures::web_socket_stream::WebSocketWriterExt;
use crate::enums::app_error::AppError;

pub struct Connection<R:Read, W:Write> {
    pub reader: WebSocketReader<R>,
    pub writer: WebSocketWriterExt<W>,
    pub offer_catalog: Arc<RwLock<OfferCatalog>>,
    pub notification_hub: Arc<RwLock<NotificationHub>>,
    pub channel: Option<PeerChannel>,
}

impl<R:Read, W:Write> Connection<R, W> {
    pub fn connection_loop(&mut self) -> Result<(), AppError>
    {
        loop {
            match self.reader.read_web_socket_message() {
                Ok(data) => {
                    if let Err(err) = self.process_incoming_message_data(&data) {
                        self.handle_error(err)?
                    }
                },
                Err(error) => {
                    match error {
                        WebSocketError::IO(io::ErrorKind::WouldBlock) => match self.check_channel() {
                            Err(error) => match error {
                                AppError::ChannelReadTimeout => (),
                                _ => {
                                    self.handle_error(error)?;
                                }
                            }
                            _ => ()
                        },
                        _ => {
                            self.handle_error(error.into())?;
                        }
                    }
                }
            };
        }
    }

    fn check_channel(&mut self) -> Result<(), AppError> {
        if let Some(channel) = &self.channel {
            let message: NotificationMessage = channel.receiver.try_recv()?;

            match message {
                NotificationMessage::Offer(_, message) => self.process_offer_message(message)?,
                NotificationMessage::Answer(_, message) => self.process_answer_message(message)?,
                NotificationMessage::CancelOffer(_, message) => self.process_cancel_offer_message(message)?,
                NotificationMessage::RejectOffer(_, message) => self.process_reject_offer_message(message)?,
                NotificationMessage::NewIceCandidate(_, message) => self.process_new_ice_candidate_message(message)?,
                NotificationMessage::TerminateChannel(name) => {
                    if name == channel.name {
                        return Err(AppError::Terminate);
                    } else {
                        return Err(AppError::TargetChannelDoesNotExist);
                    }
                },
                NotificationMessage::Terminate => return Err(AppError::Terminate),
            }
        }

        Ok(())
    }

    fn process_incoming_message_data(&mut self, data: &[u8]) -> Result<(), AppError> {
        let text = match str::from_utf8(data) {
            Ok(text) => text,
            Err(_) => return Ok(())
        };


        let json = JSON::parse(text)?;
        let error = AppError::Json(JSONError::Parse);

        let message_type = match json {
            JSON::JSONObject(ref members) => match members.get("type") {
                Some(JSON::JSONString(value)) =>  MessageType::from_str(value.as_str())?,
                _ => return Err(error)
            }
            _ => return Err(error)
        };
        println!("incoming message: {:?}", message_type);

        match message_type {
            MessageType::GetOffers =>
                self.process_get_offers_message(GetOffersMessage::from_json(json)?)?,
            MessageType::Offer =>
                self.process_offer_message(OfferMessage::from_json(json)?)?,
            MessageType::Answer =>
                self.process_answer_message(OfferMessage::from_json(json)?)?,
            MessageType::OfferReject =>
                self.process_reject_offer_message(RejectOfferMessage::from_json(json)?)?,
            MessageType::OfferCancel =>
                self.process_cancel_offer_message(CancelOfferMessage::from_json(json)?)?,
            MessageType::NewIceCandidate =>
                self.process_new_ice_candidate_message(NewIceCandidateMessage::from_json(json)?)?,
            MessageType::GetOffersResponse =>
                return Err(AppError::UnexpectedMessageType),
        }

        Ok(())
    }

    fn register_channel(&mut self, name: &str) {
        let channel = self.notification_hub.write().unwrap().register_channel(name);
        self.channel = Some(channel);
    }

    fn process_get_offers_message(&mut self, message: GetOffersMessage) -> Result<(), AppError> {
        let originator = message.originator.as_str();
        if let Some(channel) = &self.channel {
            if channel.name != originator {
                channel.sender.send(NotificationMessage::TerminateChannel(channel.name.clone()))?;
                self.register_channel(originator);
            }
        } else {
            self.register_channel(originator);
        }

        let offer_catalog = self.offer_catalog.read().unwrap();
        let offers = offer_catalog.get_offers_for(originator);

        let response = GetOffersResponseMessage {
            message_id: message.message_id,
            message_type: MessageType::GetOffersResponse,
            offers,
        }.to_json();

        let response = response.stringify();
        self.writer.send_web_socket_message(Message::Text(response.as_bytes()))?;
        Ok(())
    }

    fn process_offer_message(&mut self, message: OfferMessage) -> Result<(), AppError> {
        if let Some(channel) = &self.channel {
            if channel.name == message.originator {
                let offer_catalog: &Arc<RwLock<OfferCatalog>> = &self.offer_catalog;
                let offer = offer_util::create_offer_from_message(&message)?;
                offer_catalog.write().unwrap().insert_request(offer)?;
                channel.sender.send(NotificationMessage::Offer(channel.name.clone(), message))?;
            } else {
                let response = message.to_json().stringify();
                self.writer.send_web_socket_message(Message::Text(response.as_bytes()))?;
            }
        }

        Ok(())
    }

    fn process_answer_message(&mut self, message: OfferMessage) -> Result<(), AppError> {
        if let Some(channel) = &self.channel {
            if channel.name == message.originator {
                let offer_catalog: &Arc<RwLock<OfferCatalog>> = &self.offer_catalog;
                offer_catalog.write().unwrap().remove_offer(message.counterpart.as_str(), message.originator.as_str())?;
                channel.sender.send(NotificationMessage::Answer(channel.name.clone(), message))?;
            } else {
                let response = message.to_json().stringify();
                self.writer.send_web_socket_message(Message::Text(response.as_bytes()))?;
            }
        }

        Ok(())
    }

    fn process_reject_offer_message(&mut self, message: RejectOfferMessage) -> Result<(), AppError> {
        if let Some(channel) = &self.channel {
            if channel.name == message.originator {
//            let offer_catalog: &Arc<RwLock<OfferCatalog>> = &context.offer_catalog;
//            offer_catalog.write().unwrap().remove_offer(message.counterpart.as_str(), message.originator.as_str())?;
//            channel.sender.send(NotificationMessage::RejectOffer(channel.name.clone(), message))?;
            } else {
                let response = message.to_json().stringify();
                self.writer.send_web_socket_message(Message::Text(response.as_bytes()))?;
            }
        }

        Ok(())
    }

    fn process_cancel_offer_message(&mut self, message: CancelOfferMessage) -> Result<(), AppError> {
        if let Some(channel) = &self.channel {
            if channel.name == message.originator {
                let offer_catalog: &Arc<RwLock<OfferCatalog>> = &self.offer_catalog;
                offer_catalog.write().unwrap().remove_offer(message.originator.as_str(), message.counterpart.as_str())?;
                channel.sender.send(NotificationMessage::CancelOffer(channel.name.clone(), message))?;
            } else {
                let response = message.to_json().stringify();
                self.writer.send_web_socket_message(Message::Text(response.as_bytes()))?;
            }
        }

        Ok(())
    }

    fn process_new_ice_candidate_message(&mut self, message: NewIceCandidateMessage) -> Result<(), AppError> {
        if let Some(channel) = &self.channel {
            if channel.name == message.originator {
                channel.sender.send(NotificationMessage::NewIceCandidate(channel.name.clone(), message))?;
            } else {
                let response = message.to_json().stringify();
                self.writer.send_web_socket_message(Message::Text(response.as_bytes()))?;
            }
        }

        Ok(())
    }

    fn handle_error(&mut self, error: AppError) -> Result<(), AppError> {
        match error {
            AppError::Terminate => {
                self.writer.send_web_socket_message(Message::Close(format!("{:?}", error).as_bytes()))?;
            },
            _ => {
                ()
            }
        }

        Err(error)
    }
}