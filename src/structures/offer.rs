use std::collections::HashMap;
use crate::enums::offer_type::*;
use json::JSON;

#[derive(Debug, PartialEq)]
pub struct Offer {
    pub offer_type: OfferType,
    pub originator: String,
    pub counterpart: String,
    pub sdp: JSON,
}

impl Offer {

    pub fn to_json(&self) -> JSON{
        let mut members: HashMap<String, JSON> = HashMap::new();
        if self.offer_type == OfferType::Request {
            members.insert(String::from("type"), JSON::JSONString(OFFER_TYPE_REQUEST.to_string()));
        }
        else if self.offer_type == OfferType::Answer {
            members.insert(String::from("type"), JSON::JSONString(OFFER_TYPE_ANSWER.to_string()));
        }
        members.insert(String::from("originator"), JSON::JSONString(self.originator.clone()));
        members.insert(String::from("counterpart"), JSON::JSONString(self.counterpart.clone()));
        members.insert(String::from("sdp"), self.sdp.clone());

        JSON::JSONObject(members)
    }
}