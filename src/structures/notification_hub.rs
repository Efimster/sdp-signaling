use std::sync::mpsc;
use std::collections::HashMap;
use std::thread;
use std::sync::{Arc, Mutex, RwLock};
use crate::structures::messages::offer_message::OfferMessage;
use crate::structures::messages::cancel_offer_message::CancelOfferMessage;
use crate::structures::messages::reject_offer_message::RejectOfferMessage;
use crate::structures::messages::new_ice_candidate_message::NewIceCandidateMessage;
use crate::enums::app_error::AppError;

pub struct NotificationHub {
    named_channels: HashMap<String, HubChannel>,
    sender: Mutex<mpsc::Sender<Message>>,
    thread: Option<thread::JoinHandle<()>>,
}

pub struct PeerChannel {
    pub name: String,
    pub sender: mpsc::Sender<Message>,
    pub receiver: mpsc::Receiver<Message>
}

struct HubChannel {
    sender: Mutex<mpsc::Sender<Message>>,
}

#[derive(Debug)]
pub enum Message {
    Offer(String, OfferMessage),
    Answer(String, OfferMessage),
    CancelOffer(String, CancelOfferMessage),
    RejectOffer(String, RejectOfferMessage),
    NewIceCandidate(String, NewIceCandidateMessage),
    TerminateChannel(String),
    Terminate
}


impl NotificationHub {

    pub fn create() -> Arc<RwLock<NotificationHub>> {
        let (sender, receiver) = mpsc::channel();
        let sender = Mutex::new(sender);
        let hub = NotificationHub {
            sender,
            named_channels: HashMap::new(),
            thread: None,
        };

        let hub = Arc::new(RwLock::new(hub));
        let hub_ref = Arc::clone(&hub);

        let thread = thread::spawn(move ||{
        let mut terminate = false;
            loop {
                let message = receiver.recv().unwrap();
                //println!("NotificationHub got message: {:?}", message);


                let result = match message {
                    Message::Offer(from, message) => hub_ref.read().unwrap().process_offer_message(&from, message),
                    Message::Answer(from, message) => hub_ref.read().unwrap().process_answer_message(&from, message),
                    Message::CancelOffer(from, message) => hub_ref.read().unwrap().process_cancel_message(&from, message),
                    Message::RejectOffer(from, message) => hub_ref.read().unwrap().process_reject_message(&from, message),
                    Message::NewIceCandidate(from, message) => hub_ref.read().unwrap().process_new_ice_candidate_message(&from, message),
                    Message::TerminateChannel(name) => hub_ref.write().unwrap().process_terminate_channel(&name),
                    Message::Terminate => {
                        terminate = true;
                        hub_ref.write().unwrap().process_terminate()
                    },
                };

                if let Err(error) = result {
                    eprintln!("NotificationHub error: {:?}", error)
                }

                if terminate {
                    break;
                }
            }

            hub_ref.write().unwrap().thread = None;
        });
        hub.write().unwrap().thread = Some(thread);
        hub
    }

    pub fn register_channel(&mut self, name:&str) -> PeerChannel {
        let (to_party_tx, to_party_rx) = mpsc::channel();
        let to_hub_tx =  self.sender.lock().unwrap().clone();

        let hub_channel = HubChannel{
            sender:Mutex::new(to_party_tx),
        };
        let party_channel = PeerChannel {
            name:name.to_string(),
            sender: to_hub_tx,
            receiver: to_party_rx
        };

        self.named_channels.insert(name.to_string(), hub_channel);
        println!("register_channel \"{}\"", name);
        party_channel
    }

    fn process_offer_message(&self, from: &str, message: OfferMessage) -> Result<(), AppError> {
        if &message.counterpart == from {
            return Err(AppError::TargetChannelLoop);
        }

        match self.named_channels.get(&message.counterpart) {
            Some(channel) => channel.sender.lock().unwrap().send(Message::Offer(from.to_string(), message))?,
            None => return Err(AppError::TargetChannelDoesNotExist),
        }

        Ok(())
    }

    fn process_answer_message(&self, from: &str, message: OfferMessage) -> Result<(), AppError>{
        if &message.counterpart == from {
            return Err(AppError::TargetChannelLoop);
        }

        match self.named_channels.get(&message.counterpart) {
            Some(channel) => channel.sender.lock().unwrap().send(Message::Answer(from.to_string(), message))?,
            None => return Err(AppError::TargetChannelDoesNotExist),
        }

        Ok(())
    }

    fn process_cancel_message(&self, from: &str, message: CancelOfferMessage) -> Result<(), AppError> {
        if &message.counterpart == from {
            return Err(AppError::TargetChannelLoop);
        }

        match self.named_channels.get(&message.counterpart) {
            Some(channel) => channel.sender.lock().unwrap().send(Message::CancelOffer(from.to_string(), message))?,
            None => return Err(AppError::TargetChannelDoesNotExist)
        }

        Ok(())
    }

    fn process_reject_message(&self, from: &str, message: RejectOfferMessage) -> Result<(), AppError> {
        if message.counterpart == from {
            return Err(AppError::TargetChannelLoop);
        }

        match self.named_channels.get(&message.counterpart) {
            Some(channel) => channel.sender.lock().unwrap().send(Message::RejectOffer(from.to_string(), message))?,
            None => return Err(AppError::TargetChannelDoesNotExist),
        }

        Ok(())
    }

    fn process_new_ice_candidate_message(&self, from: &str, message: NewIceCandidateMessage) -> Result<(), AppError> {
        if message.counterpart == from {
            return Err(AppError::TargetChannelLoop);
        }

        match self.named_channels.get(&message.counterpart) {
            Some(channel) => channel.sender.lock().unwrap().send(Message::NewIceCandidate(from.to_string(), message))?,
            None => return Err(AppError::TargetChannelDoesNotExist),
        }

        Ok(())
    }

    fn process_terminate_channel(&mut self, name: &str) -> Result<(), AppError>  {
        match self.named_channels.remove(name) {
            Some(_) => Ok(()),
            None => Err(AppError::TargetChannelDoesNotExist)
        }
    }

    fn process_terminate(&mut self) -> Result<(), AppError> {
        let keys:Vec<String> = self.named_channels.keys().map(|key|key.clone()).collect::<Vec<_>>();
        let mut last_error: Option<AppError> = None;

        for channel in keys {
            if let Err(error) = self.process_terminate_channel( channel.as_str()) {
                last_error = Some(error);
            }
        }

        match last_error {
            Some(error) => Err(error),
            None => Ok(())
        }
    }

    pub fn terminate(&self) {
        self.sender.lock().unwrap().send(Message::Terminate).unwrap();
    }

}

#[cfg(test)]
mod tests {
    use super::*;
    use std::time::Duration;
    use crate::enums::message_type::MessageType;
    use json::JSON;

    #[test]
    fn test_create_hub(){
        let hub = NotificationHub::create();
        let channel = hub.write().unwrap().register_channel("channel1");
        channel.sender.send(Message::TerminateChannel("xxx".to_string())).unwrap();
        {
            let named_channels: &HashMap<String, HubChannel> = &hub.read().unwrap().named_channels;
            let hub_channel = named_channels.get("channel1").unwrap();
            assert_eq!(channel.name, "channel1");
            hub_channel.sender.lock().unwrap().send(Message::Terminate).unwrap();
        }

        let message = channel.receiver.recv().unwrap();
        match message {
            Message::Terminate => (),
            _ => panic!("unexpected message")
        }
   }

    #[test]
    fn test_offer_answer_message() {
        let hub = NotificationHub::create();
        let nick_channel = hub.write().unwrap().register_channel("nick");
        let vick_channel = hub.write().unwrap().register_channel("vick");
        let sdp = "{\"sdp\":\"v = 0\r{}}\r\n\"}";

        let message = OfferMessage {
            message_id: 1,
            message_type: MessageType::Offer,
            originator: "nick".to_string(),
            counterpart: "vick".to_string(),
            sdp: JSON::parse(sdp).unwrap(),
        };

        nick_channel.sender.send(Message::Offer("nick".to_string(), message)).unwrap();
        let message = vick_channel.receiver.recv().unwrap();
        match message {
            Message::Offer(_, message) => {
                assert_eq!(message.originator, "nick");
                assert_eq!(message.counterpart, "vick");
            },
            _ => panic!("unexpected message")
        }

        let sdp2 = "{\"sdp2\":\"v222 = 220\r{}}\r\n\"}";
        let message = OfferMessage {
            message_id: 2,
            message_type: MessageType::Answer,
            originator: "vick".to_string(),
            counterpart: "nick".to_string(),
            sdp: JSON::parse(sdp2).unwrap(),
        };

        vick_channel.sender.send(Message::Answer("vick".to_string(), message)).unwrap();
        let message = nick_channel.receiver.recv().unwrap();
        match message {
            Message::Answer(_, message) => {
                assert_eq!(message.originator, "vick");
                assert_eq!(message.counterpart, "nick");
                assert_eq!(message.sdp.stringify(), sdp2);
            },
            _ => panic!("unexpected message")
        }

    }

    #[test]
    fn test_terminate_channel_message() {
        let hub = NotificationHub::create();
        let nick_channel = hub.write().unwrap().register_channel("nick");
        let vick_channel = hub.write().unwrap().register_channel("vick");
        nick_channel.sender.send(Message::TerminateChannel("nick".to_string())).unwrap();
        thread::sleep(Duration::from_millis(1));
        {
            let named_channels: &HashMap<String, HubChannel> = &hub.read().unwrap().named_channels;
            match named_channels.get("nick") {
                None => (),
                Some(_) => panic!("channel should be removed")
            }
        }

        let sdp2 = "{\"sdp2\":\"vdsfsdf222 = 0\r{}}\r\n\"}";
        let message = OfferMessage {
            message_id: 2,
            message_type: MessageType::Answer,
            originator: "vick".to_string(),
            counterpart: "nick".to_string(),
            sdp: JSON::parse(sdp2).unwrap(),
        };

        vick_channel.sender.send(Message::Answer("vick".to_string(), message)).unwrap();
        match nick_channel.receiver.recv() {
            Ok(_) => panic!("unexpected result"),
            Err(_) => ()
        }
    }

    #[test]
    fn test_terminate_message() {
        let hub = NotificationHub::create();
        let nick_channel = hub.write().unwrap().register_channel("nick");
        let sdp = "{\"sdp\":\"v = 0\r{}}\r\n\"}";
        let message = OfferMessage {
            message_id: 1,
            message_type: MessageType::Offer,
            originator: "nick".to_string(),
            counterpart: "vick".to_string(),
            sdp: JSON::parse(sdp).unwrap(),
        };

        hub.read().unwrap().sender.lock().unwrap().send(Message::Terminate).unwrap();
        thread::sleep(Duration::from_millis(1));
        {
            let named_channels: &HashMap<String, HubChannel> = &hub.read().unwrap().named_channels;
            assert_eq!(named_channels.len(), 0);
        }

        assert!(nick_channel.sender.send(Message::Offer("nick".to_string(), message)).is_err());
    }

}