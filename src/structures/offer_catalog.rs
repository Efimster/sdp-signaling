use std::collections::HashMap;
use std::sync::Arc;
use crate::structures::offer::Offer;
use crate::structures::app_error::*;

///outgoing: originator to "hashmap of counterpart to offer"
///incoming: counterpart to "hashmap of originator to offer"
pub struct OfferCatalog {

    pub incoming: HashMap<String, HashMap<String, Arc<Offer>>>,
    outgoing: HashMap<String, HashMap<String, Arc<Offer>>>,
}

impl OfferCatalog {

    pub fn create() -> OfferCatalog {
        let incoming= HashMap::new();
        let outgoing= HashMap::new();

        OfferCatalog{
            incoming,
            outgoing
        }
    }

    ///adds offer to catalog
    pub fn insert_request(&mut self, offer:Offer) -> Result<(), AppError>{

        let offer = Arc::new(offer);
        let offer_clone = Arc::clone(&offer);

        match self.outgoing.get_mut(offer.originator.as_str()) {
            None => {
                let mut counterparts:HashMap<String, Arc<Offer>> = HashMap::new();
                let originator = offer.originator.clone();
                let counterpart = offer.counterpart.clone();
                counterparts.insert(counterpart, offer);
                self.outgoing.insert(originator, counterparts);
            },
            Some(counterparts) => {
                let counterpart = offer.counterpart.clone();
                counterparts.insert(counterpart, offer);
            }
        }

        match self.incoming.get_mut(&offer_clone.counterpart) {
            None => {
                let mut originators: HashMap<String, Arc<Offer>> = HashMap::new();
                let originator = offer_clone.originator.clone();
                let counterpart = offer_clone.counterpart.clone();
                println!("inserting {:?} to {} incomings", &offer_clone, counterpart);
                ;
                originators.insert(originator, offer_clone);

                self.incoming.insert(counterpart, originators);
            },
            Some(originators) => {
                let originator = offer_clone.originator.clone();
                originators.insert(originator, offer_clone);
            }
        }
        Ok(())
    }

    ///removes paired offer from catalog
    pub fn remove_offer(&mut self, originator: &str, counterpart: &str) -> Result<(), AppError> {
        let error = AppError{kind: AppErrorKind::Offer, message: "offer not found".to_string()};

        if let Some(originators) = self.incoming.get_mut(counterpart){
            match originators.remove(originator) {
                None => return Err(AppError { kind: AppErrorKind::Offer, message: "offer not found1111".to_string() }),
                Some(_) => ()
            }

            if originators.len() == 0 {
                self.incoming.remove(counterpart);
            }
        }

        if let Some(counterparts) = self.outgoing.get_mut(originator){
            match counterparts.remove(counterpart) {
                None => return Err(error),
                Some(_) => ()
            }
            if counterparts.len() == 0 {
                self.outgoing.remove(originator);
            }
        }

        Ok(())
    }

    pub fn get_offers_for(&self, name: &str) -> Vec<Arc<Offer>> {
        let mut offers = vec![];
        if let Some(originators) = self.incoming.get(name) {
            originators.iter().for_each(|item| offers.push(item.1.clone()));
        }

        if let Some(counterparts) = self.outgoing.get(name) {
            counterparts.iter().for_each(|item| offers.push(item.1.clone()));
        }

        offers
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::enums::offer_type::OfferType;
    use crate::enums::json::JSON;

    #[test]
    fn test_insert_offer_from_non_existing_originator(){
        let mut catalog = OfferCatalog::create();
        let originator = "nick";
        let counterpart = "vick";
        let offer = Offer{
            offer_type: OfferType::Request,
            originator: String::from(originator),
            counterpart: String::from(counterpart),
            sdp: JSON::Null,
        };

        catalog.insert_request(offer).unwrap();
        assert_eq!(catalog.outgoing.len(), 1);
        assert_eq!(catalog.incoming.len(), 1);
        let counterparts = catalog.outgoing.get(originator).unwrap();
        assert_eq!(counterparts.len(), 1);
        let offer_in_outgoing = counterparts.get(counterpart).unwrap();
        assert_eq!(Arc::strong_count(offer_in_outgoing), 2);
        assert_eq!(offer_in_outgoing.originator, originator);
        assert_eq!(offer_in_outgoing.counterpart, counterpart);

        let originators = catalog.incoming.get(counterpart).unwrap();
        assert_eq!(originators.len(), 1);
        let offer_in_incoming = originators.get(originator).unwrap();
        assert_eq!(Arc::strong_count(offer_in_incoming), 2);
        assert_eq!(offer_in_incoming, offer_in_outgoing);
    }

    #[test]
    fn test_insert_offer_from_existing_originator() {
        let mut catalog = OfferCatalog::create();
        let originator = "nick";
        let counterpart = "vick";
        let offer = Offer {
            offer_type: OfferType::Request,
            originator: String::from(originator),
            counterpart: String::from(counterpart),
            sdp: JSON::Null,
        };
        catalog.insert_request(offer).unwrap();

        let counterpart2 = "dick";
        let offer2 = Offer {
            offer_type: OfferType::Request,
            originator: String::from(originator),
            counterpart: String::from(counterpart2),
            sdp: JSON::Null,
        };
        catalog.insert_request(offer2).unwrap();

        assert_eq!(catalog.outgoing.len(), 1);
        assert_eq!(catalog.incoming.len(), 2);
        let counterparts = catalog.outgoing.get(originator).unwrap();
        assert_eq!(counterparts.len(), 2);
        let offer_in_outgoing = counterparts.get(counterpart2).unwrap();
        assert_eq!(Arc::strong_count(offer_in_outgoing), 2);
        assert_eq!(offer_in_outgoing.originator, originator);
        assert_eq!(offer_in_outgoing.counterpart, counterpart2);
        let originators = catalog.incoming.get(counterpart).unwrap();
        assert_eq!(originators.len(), 1);

        let originators = catalog.incoming.get(counterpart2).unwrap();
        assert_eq!(originators.len(), 1);
        let offer_in_incoming = originators.get(originator).unwrap();
        assert_eq!(Arc::strong_count(offer_in_incoming), 2);
        assert_eq!(offer_in_incoming, offer_in_outgoing);
    }

    #[test]
    fn test_update_offer_of_existing_counterpart() {
        let mut catalog = OfferCatalog::create();

        let originator = "nick";
        let counterpart = "vick";
        let offer = Offer {
            offer_type: OfferType::Request,
            originator: String::from(originator),
            counterpart: String::from(counterpart),
            sdp: JSON::parse(sdp).unwrap(),
        };
        catalog.insert_request(offer).unwrap();

        let counterparts = catalog.outgoing.get(originator).unwrap();
        let offer_in_outgoing = counterparts.get(counterpart).unwrap();
        let originators = catalog.incoming.get(counterpart).unwrap();
        let offer_in_incoming = originators.get(originator).unwrap();
        assert_eq!(Arc::strong_count(offer_in_incoming), 2);
        assert_eq!(Arc::strong_count(offer_in_outgoing), 2);

        let offer2 = Offer {
            offer_type: OfferType::Request,
            originator: String::from(originator),
            counterpart: String::from(counterpart),
            sdp: JSON::parse(sdp2).unwrap(),
        };
        catalog.insert_request(offer2).unwrap();

        let counterparts = catalog.outgoing.get(originator).unwrap();
        let offer_in_outgoing_updated = counterparts.get(counterpart).unwrap();
        let originators = catalog.incoming.get(counterpart).unwrap();
        let offer_in_incoming_updated = originators.get(originator).unwrap();
        assert_eq!(Arc::strong_count(offer_in_outgoing_updated), 2);
        assert_eq!(Arc::strong_count(offer_in_incoming_updated), 2);
        assert_eq!(offer_in_outgoing_updated, offer_in_incoming_updated);
        assert_eq!(offer_in_outgoing_updated.sdp, JSON::parse(sdp2).unwrap());
    }

    #[test]
    fn test_pair_offer() {
        let mut catalog = OfferCatalog::create();
        let originator = "nick";
        let counterpart = "vick";
        let offer = Offer {
            offer_type: OfferType::Request,
            originator: String::from(originator),
            counterpart: String::from(counterpart),
            sdp: JSON::Null,
        };
        catalog.insert_request(offer).unwrap();

        assert_eq!(catalog.outgoing.len(), 1);
        assert_eq!(catalog.incoming.len(), 1);

        catalog.remove_offer(originator, counterpart).unwrap();

        assert_eq!(catalog.outgoing.len(), 0);
        assert_eq!(catalog.incoming.len(), 0);

        let offer = Offer {
            offer_type: OfferType::Request,
            originator: String::from(originator),
            counterpart: String::from(counterpart),
            sdp: JSON::Null,
        };
        catalog.insert_request(offer).unwrap();

        assert_eq!(catalog.outgoing.len(), 1);
        assert_eq!(catalog.incoming.len(), 1);

        catalog.remove_offer(originator, counterpart).unwrap();

        assert_eq!(catalog.outgoing.len(), 0);
        assert_eq!(catalog.incoming.len(), 0);
    }

    #[test]
    fn test_get_incoming_offers() {
        let mut catalog = OfferCatalog::create();
        assert_eq!(catalog.get_offers_for("vick").len(), 0);

        let offer = Offer {
            offer_type: OfferType::Request,
            originator: String::from("nick"),
            counterpart: String::from("vick"),
            sdp: JSON::Null,
        };

        let offer2 = Offer {
            offer_type: OfferType::Request,
            originator: String::from("john"),
            counterpart: String::from("vick"),
            sdp: JSON::Null,
        };

        catalog.insert_request(offer).unwrap();
        catalog.insert_request(offer2).unwrap();

        let offers = catalog.get_offers_for("vick");

        for offer in offers.iter() {
            assert_eq!(offer.counterpart, "vick");
        }
        let offers = offers.iter().map(|offer| offer.to_json()).collect::<Vec<_>>();
        assert_eq!(offers.len(), 2);
    }
}